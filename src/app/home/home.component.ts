import { Component, OnInit } from '@angular/core';
import { APIService } from '../api/service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public state_name: string = null;
  public city_name: string = null;
  public listings: any[] = null;

  constructor(private apiService: APIService, private route: ActivatedRoute, private router: Router ) { }

  ngOnInit() {
    //-- get location parameters from URL
    let params = this.route.snapshot.params;
    if (params && params.state_name ||  ( params.state_name && params.city_name ) ) {
      if (params.state_name) this.state_name = params.state_name;
      if (params.city_name) this.city_name = params.city_name; 
    } else {
      this.router.navigate(['colorado','fort-collins']);
    }

    //-- load listings
    //-- TODO: submit state and city as variables to filter results
    this.apiService.listings.getList().subscribe( (res) => this.listings = res.data.restaurants );
  }

  public genres = [
    'Restaurants',
    'Fast Food',
    'Sandwiches',
    'Mexican',
    'Food',
    'Nightlife',
    'Bars',
    'American (Traditional)',
    'Pizza',
    'Burgers',
    'Chicken Wings',
    'Chinese',
    'Breakfast & Brunch',
    'American',
    'Italian',
    'Salad',
    'Seafood',
    'Sushi Bars',
    'Japanese',
    'Delis',
    'Coffee & Tea',
    'Barbeque',
    'Sports Bars',
    'Tex-Mex',
    'Indian',
    'Thai',
    'Asian Fusion',
    'Cafes',
    'Southern',
    'Steakhouses',
    'Diners',
    'Mediterranean',
    'Bakeries',
    'Korean',
    'Latin American',
    'Desserts',
    'Vietnamese',
    'Food Trucks',
    'Soup',
    'Buffets',
    'Event Planning & Services',
    'Greek',
    'Cocktail Bars',
    'Caribbean',
    'Wine Bars',
    'Juice Bars & Smoothies',
    'Gastropubs',
    'Middle Eastern',
    'Cajun/Creole',
    'Caterers',
    'Vegetarian',
    'Arts & Entertainment',
    'Chicken Shop',
    'Hot Dogs',
    'Cuban',
    'Specialty Food',
    'Comfort Food',
    'Halal',
    'Tapas/Small Plates',
    'Gluten-Free',
    'Ice Cream & Frozen Yogurt',
    'Noodles',
    'Tacos',
    'Vegan',
    'Brazilian',
    'French',
    'Pakistani',
    'Cheesesteaks',
    'Grocery',
    'Poke',
    'Pubs',
    'Bagels',
    'Soul Food',
    'Persian/Iranian',
    'African',
    'Beer Bar',
    'Hookah Bars',
    'Peruvian',
    'Ramen',
    'Salvadoran',
    'Lounges',
    'Venues & Event Spaces',
    'Wraps',
    'Colombian',
    'Beer, Wine & Spirits',
    'British',
    'Dive Bars',
    'Food Stands',
    'Hot Pot',
    'Malaysian',
    'Meat Shops',
    'Szechuan',
    'Tapas Bars'
  ]

}
