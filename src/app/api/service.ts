import {Injectable} from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable()
export class APIService {
    private API_URL = 'https://admin.credibytes.com/v1alpha1/graphql';
    private options: RequestOptions = new RequestOptions({ 
        headers: new Headers({
            'x-hasura-admin-secret': 'Zest_Or_Nothing13#',
            'Content-Type': 'application/json', 
            'Accept': 'application/json',
        }), 
        withCredentials: true });
    constructor(private http: Http){}
    public test = { 
        getProfiles: () => this.post({query: 'query { profile { id, name } }', variables: null })
    }
    public listings = {
       //getOne: (id) => this.get(`/restaurants/${id}`),
        getList: () => this.post({query: 'query { restaurants { id, name, url_name } }', variables: null })
    }
    public user = {};
    public reviews = {};
    public menus = {};
    private get(endpoint: string) {
        return this.http.get(this.API_URL + endpoint);
    }
    private post(body: any) {
        return this.http.post(this.API_URL,body, this.options).pipe( 
            map( (res: Response) => res.json() )
        )
    }
}