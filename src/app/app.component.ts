import { Component, PLATFORM_ID } from '@angular/core';
import { AuthService } from './auth/service';
import { APIService } from './api/service';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';  
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({  
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'credibytes';
  
  private subs: Subscription[] = [];
  
  constructor(private api: APIService, private authService: AuthService){
    if (isPlatformBrowser) {
      this.authService.handleAuthentication();
    }
  }

  ngOnInit() {
    
    //this.api.restaraunts.getList().subscribe(data => this.data = data, error => this.data = error);
    if (isPlatformBrowser) {
      // if (navigator.geolocation) {
      //   navigator.geolocation.getCurrentPosition( (position) => this.location = position);
      // } else {
      //   // no can do
      // }
    }
  }
  public login() {
    if (isPlatformBrowser) {
      console.log('login');
      this.authService.login();
    }
  }
}
