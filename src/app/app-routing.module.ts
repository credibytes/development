import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LocationComponent } from './location/location.component';
import { ListingComponent } from './listing/listing.component';
import { CategoryComponent } from './category/category.component';
import { AuthComponent } from './auth/authenticate/component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'authenticate',
    component: AuthComponent
  },
  // {
  //   path: 'about',
  //   component: AboutComponent
  // }
  // {
  //   path: 'cities',
  //   component: CitiesComponent
  // },
  // {
  //   path: 'legal',
  //   component: LegalComponent
  // },
  {
    path: ':state_name',
    component: HomeComponent,
    children: [
      {
        path: 'listing/:listing_name',
        component: ListingComponent
      },
      {
        path: 'category/:category_name',
        component: CategoryComponent
      },
      // {
      //   path: 'blog',
      //   component: BlogComponent
      // },
      // {
      //   path: 'blog/:post_name',
      //   component: BlogComponent
      // }

    ]
  },
  {
    path: ':state_name/:city_name',
    component: HomeComponent,
  },
  {
    path: ':state_name/:city_name/listing/:listing_name',
    component: ListingComponent
  },
  {
    path: ':state_name/:city_name/category/:category_name',
    component: CategoryComponent
  },
      // {
      //   path: 'blog',
      //   component: BlogComponent
      // },
      // {
      //   path: 'blog/:post_name',
      //   component: BlogComponent
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
