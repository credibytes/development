import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';

//Services
import {APIService} from './api/service';
import {AuthService} from './auth/service';

//Components
import { AuthComponent } from './auth/authenticate/component';
import { AppModule } from './app.module';


@NgModule({
  imports: [
    
    AppRoutingModule,
    HttpModule,
    AppModule,
    BrowserTransferStateModule
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppBrowserModule { }
