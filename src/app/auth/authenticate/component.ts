import { Component } from '@angular/core';
import  { AuthService } from '../service';
import { ActivatedRouteSnapshot } from '@angular/router';

@Component({
    selector: 'auth',
    template:
    `
        Authenticating...
    `,
    styleUrls: ['./component.css']
  })
  export class AuthComponent {
    constructor(public auth: AuthService) {
        auth.handleAuthentication();
      }
    
      ngOnInit() {
        if (this.auth.isAuthenticated()) {
          this.auth.renewTokens();
        }
      }
  }