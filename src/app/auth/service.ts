import {Injectable, Inject, PLATFORM_ID} from '@angular/core';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import * as auth0 from 'auth0-js';
import {BehaviorSubject} from 'rxjs';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';  

@Injectable()
export class AuthService {
    private clientSecret = 'au_QnZ80pLb4EN2sTk9W81vRXJ_VSmhJpOwIXcNAqdylE1QPKPKBOaeqDapImsYR';
    
    private auth0 = new auth0.WebAuth({
        clientID: 'vUfB0a6amKpFFH3qoVrit3vvcvMmn2iz',
        domain: 'credibytes1.auth0.com',
        responseType: 'token id_token',
        redirectUri: `${environment.baseUrl}authenticate`,
        scope: 'openid'
      });
      
      constructor(@Inject(PLATFORM_ID) private platformId: Object, public router: Router, @Inject(LOCAL_STORAGE) private localStorage: any) {
      }
      
      public auth$ = new BehaviorSubject<boolean>(false);
      public login(): void {
        this.auth0.authorize();
      }

      public handleAuthentication(): void {
          //auth0.parseHash fails on server platform because theres no window object
          if (isPlatformBrowser) {
            this.auth0.parseHash((err, authResult) => {
              if (authResult && authResult.accessToken && authResult.idToken) {
                //TODO check to see when this is resolved: https://github.com/maciejtreder/ng-toolkit/issues/515
                window.location.hash = '';
                this.setSession(authResult);
                this.router.navigate([environment.baseUrl]);
              } else if (err) {
                this.router.navigate([environment.baseUrl]);
                console.log(err);
              }
            });
          }
      }
      public renewTokens(): void {
        this.auth0.checkSession({}, (err: auth0.Auth0Error, authResult) => {
          if (authResult && authResult.accessToken && authResult.idToken) {
            this.setSession(authResult);
          } else if (err) {
            alert(`Could not get a new token (${err.error}: ${err.errorDescription}).`);
            this.logout();
          }
        });
      }
      private setSession(authResult): void {
        // Set the time that the Access Token will expire at
        const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
        this.localStorage.setItem('access_token', authResult.accessToken);
        this.localStorage.setItem('id_token', authResult.idToken);
        this.localStorage.setItem('expires_at', expiresAt);
      }
    
      public logout(): void {
        // Remove tokens and expiry time from localStorage
        this.auth$.next(false);
        this.localStorage.removeItem('access_token');
        this.localStorage.removeItem('id_token');
        this.localStorage.removeItem('expires_at');
        // Go back to the home route
        this.router.navigate(['/']);
      }
    
      public isAuthenticated(): boolean {
        // Check whether the current time is past the
        // Access Token's expiry time
        const expiresAt = JSON.parse(this.localStorage.getItem('expires_at') || '{}');
        return new Date().getTime() < expiresAt;
      }

}