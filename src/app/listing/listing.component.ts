import { Component, OnInit } from '@angular/core';
import { APIService } from '../api/service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {
  public listing: any = null;
  public state_name: string = null;
  public city_name: string = null;
  constructor( private apiService: APIService, private route: ActivatedRoute, private router: Router ) { }

  ngOnInit() {
    let params = this.route.snapshot.params;
    if ( params && params.state_name && params.city_name && params.listing_name) {
      this.apiService.listings.getList().subscribe( res => {
        this.listing = res.data.restaurants.find( l => l.url_name === this.route.snapshot.params.listing_name);
      });
    } else {
      this.router.navigate(['/']);
    }
  }

}
